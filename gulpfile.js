
const gulp          = require('gulp');
const autoprefixer  = require('gulp-autoprefixer');
const cleanCSS      = require('gulp-clean-css');
const sass          = require('gulp-sass');
const sassGlob      = require('gulp-sass-glob');
const browserSync   = require('browser-sync').create();

function scss() {
    return gulp
        .src('scss/werlabs.scss')
        .pipe(sassGlob())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cleanCSS())
        .pipe(gulp.dest('css'))
        .pipe(browserSync.stream());
}

function serve(done) {
    browserSync.init({
        server: {
            baseDir: './'
        }
    });
    done();
}

function reload(done) {
    browserSync.reload();
    done();
}

function watch() {
    gulp.watch('./scss/**/*.scss', scss);
    gulp.watch(['./index.html','./pages/*.html'], reload);
    gulp.watch('./js/*.js', reload);
}

const dev = gulp.series(scss, serve, watch);
exports.default = dev;