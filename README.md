###Description

A simple solution to kick up prototyping in the browser. Write markup with only component and utility classes and be able to quickly test design ideas or workshop on UX.


---


###How to

**Note**

This guide is myopicly written for the use on a MacOS device. Please contact peter.svegrup[at]werlabs.com if you have any questions.

--

**Requirements**

- A rough knowledge of opening and operating the terminal or equivalent command line app
- A text editor (VS Code is recommended)
- `git` installed
- `node` (and `npm`) installed

--

**Instructions**

Open the terminal, change directory to where you want the files to live

For ex `cd code` for a directory named "code" in your home directory.

--

Clone the repo into the new directory "prototype"

`git clone https://petersvegrup@bitbucket.org/werlabs/werlabs-prototyper.git prototype`

--

Change directory to "prototype"

`cd prototype`

--

Install dependencies

`npm install`

--

Start the environment

`gulp`

---

Note the following info from Browsersync on running the "gulp" command:

```
[Browsersync] Access URLs:
--------------------------------------
      Local: http://localhost:3000
   External: http://192.168.1.192:3000
--------------------------------------
```

Local: The url to visit your protyping in the browser (usually opens automatically in your default browser)

External: (*Note: Will be a different IP on your machine*) The url to visit your prototyping from any other device on the same network. In sync. As in if you scroll on your phone, it will scroll on your laptop as well. Try it, it's magic! 


--


Start editing `index.html`, adding/changing html and adding classes from utility or components.

Refer to http://plasma.werlabs.net for guidance and ready to copy code examples.


---


**@Todo**

- Make a Bitbucket repo for the Plasma styles, make it an npm package, add as a dependency and remove `scss` and `fonts` directories (fonts accessed at `werlabs.se/assets/fonts` from Plasma).


- Use `files` directive and `prepare` in `script` directive

--

https://github.com/mhulse/mhulse.github.io/wiki/npm-install-from-public-or-private-repos-on-GitHub-or-Bitbucket

https://cloudfour.com/thinks/how-to-distribute-a-pattern-library-as-an-npm-package-from-a-private-git-repo/